SRC_URI += "file://user_2020-07-07-12-40-00.cfg \
            file://user_2020-07-09-11-00-00.cfg \
            "

SRC_URI_append += " \
file://0001-debug-ad9680.patch \
file://0001-debug-txcvr.patch \
file://0001-disabled-watchdog.patch \
file://0001-disabled-watchdog-rest.patch \
file://0001-changed-resolution-and-octets.patch \
file://0001-sample-rate-and-define.patch \
file://0001-real-bits-to-14.patch \
"

FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"
