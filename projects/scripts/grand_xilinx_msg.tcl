
################################################################################
## This file contains all the message severity changes for Vivado 20xx.x.x
## These should reviewed at each release and updated if necessary

## A MUST: Every severity change must have a well defined and described role
## or purpose, and contains the instance of the original message. The main target
## here is to clean the log file from invalid CRITICAL WARNINGS.
##
## User should never change a CRITICAL WARNING to INFO, just to WARNING!


## ERROR: [DRC NSTD-1] Unspecified I/O Standard: 8 out of 48 logical ports use I/O standard (IOSTANDARD) value 'DEFAULT'
## https://www.xilinx.com/support/answers/56354.html
set_property SEVERITY {Warning} [get_drc_checks NSTD-1]
set_property SEVERITY {Warning} [get_drc_checks UCIO-1]
