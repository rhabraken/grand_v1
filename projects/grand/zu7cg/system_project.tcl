
source ../../scripts/adi_env.tcl
source $ad_hdl_dir/projects/scripts/adi_project_xilinx.tcl
source $ad_hdl_dir/projects/scripts/adi_board.tcl

#adi_project daq3_zcu102
adi_project grand_zu7cg

adi_project_files grand_zu7cg [list \
  "../common/grand_spi.v" \
  "system_top.v" \
  "system_constr.xdc"\
  "$ad_hdl_dir/library/xilinx/common/ad_iobuf.v" \
  "$ad_hdl_dir/projects/common/zu7cg/zu7cg_system_constr.xdc" ]

adi_project_run grand_zu7cg


